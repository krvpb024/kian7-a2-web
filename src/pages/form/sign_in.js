export async function POST({ request, redirect, locals }) {
  const data = await request.formData();

  const signInResponse = await fetch(
    `${import.meta.env.API_URL}/auth/sign_in`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Cookie: locals.cookieHeader,
      },
      body: JSON.stringify({
        name: data.get("name"),
        password: data.get("password"),
      }),
    }
  );
  locals.forwardSetCookieHeader(signInResponse);

  const signInResponseJsonData = await signInResponse.json();

  switch (signInResponse.status) {
    case 200:
      return redirect("/health_records", 303);
    case 400:
      switch (signInResponseJsonData.message) {
        case "account not exist":
          locals.setGlobalMessage(
            [{ type: "error", message: "帳號不存在。" }],
            {
              path: "/sign_in",
            }
          );
          return redirect("/sign_in", 303);
        case "password incorrect":
          locals.setGlobalMessage([{ type: "error", message: "密碼錯誤。" }], {
            path: "/sign_in",
          });
          return redirect("/sign_in", 303);
        default:
          break;
      }
      break;
    default:
      locals.setGlobalMessage(locals.err500Message, {
        path: "/sign_in",
      });

      return redirect("/sign_in", 303);
  }
}
