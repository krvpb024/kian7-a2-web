export async function POST({ request, redirect, locals }) {
  try {
    const data = await request.formData();

    const profileCreateResponse = await fetch(
      `${import.meta.env.API_URL}/profile`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Cookie: locals.cookieHeader,
        },
        body: JSON.stringify({
          birthDate: data.get("birthDate"),
          gender: data.get("gender"),
        }),
      }
    );

    switch (profileCreateResponse.status) {
      case 201:
        return redirect("/health_records", 303);
      case 401:
        locals.setGlobalMessage(locals.err401Message);
        return redirect("/sign_in", 303);
      default:
        break;
    }
  } catch (err) {
    const profileCreateUrl = "/profile/create";
    console.error("profile post request fail", err);
    locals.setGlobalMessage(
      [
        {
          type: "error",
          message: "建立個人檔案時伺服器錯誤，可以連絡我們或等待我們將問題排除",
        },
      ],
      {
        path: profileCreateUrl,
      }
    );
    return redirect(profileCreateUrl, 303);
  }
}
