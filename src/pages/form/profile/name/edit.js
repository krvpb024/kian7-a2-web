import * as R from "ramda";

export async function POST({ request, redirect, locals }) {
  const data = await request.formData();

  try {
    const accountNameEditResponse = await R.compose(
      R.curry(requestPatchAccountName)(locals.cookieHeader),
      JSON.stringify,
      Object.fromEntries,
      Array.from
    )(data.entries());

    const accountNameEditResponseJsonData = accountNameEditResponse.json();

    switch (accountNameEditResponse.status) {
      case 200:
        return redirect("/profile", 303);
      case 400:
        switch ((await accountNameEditResponseJsonData).message) {
          case "account has been taken":
            locals.setGlobalMessage(
              [{ type: "error", message: "帳號已被使用。" }],
              {
                path: "/profile",
              }
            );
            return redirect("/profile", 303);
          default:
            locals.setGlobalMessage(locals.err500Message);
            return redirect("/profile", 303);
        }
      case 401:
        locals.setGlobalMessage(locals.err401Message);
        return redirect("/sign_in", 303);
      default:
        throw new Error(
          `account name patch request success, but unexpected status ${accountNameEditResponse.status}.`
        );
    }
  } catch (err) {
    const redirectUrl = `/profile`;
    console.error(err);
    locals.setGlobalMessage(
      [
        {
          type: "error",
          message:
            "編輯帳號時伺服器錯誤。您可以再試一次，或是連絡我們，等待我們將問題排除",
        },
      ],
      {
        path: redirectUrl,
      }
    );
    return redirect(redirectUrl, 303);
  }
}

async function requestPatchAccountName(cookie, body) {
  return fetch(`${import.meta.env.API_URL}/account/name`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      Cookie: cookie,
    },
    body,
  });
}
