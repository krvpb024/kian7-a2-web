import * as R from "ramda";

export async function POST({ request, redirect, locals }) {
  const data = await request.formData();

  try {
    const profileEditResponse = await R.compose(
      R.curry(requestPatchProfile)(locals.cookieHeader),
      JSON.stringify,
      Object.fromEntries,
      Array.from
    )(data.entries());

    switch (profileEditResponse.status) {
      case 200:
        return redirect("/profile", 303);
      case 400:
        locals.setGlobalMessage(locals.err500Message);
        return redirect("/profile/gender/edit", 303);
      case 401:
        locals.setGlobalMessage(locals.err401Message);
        return redirect("/sign_in", 303);
      default:
        throw new Error(
          `account name patch request success, but unexpected status ${profileEditResponse.status}.`
        );
    }
  } catch (err) {
    const redirectUrl = `/profile/gender/edit`;
    console.error(err);
    locals.setGlobalMessage(
      [
        {
          type: "error",
          message:
            "編輯性別時伺服器錯誤。您可以再試一次，或是連絡我們，等待我們將問題排除",
        },
      ],
      {
        path: redirectUrl,
      }
    );
    return redirect(redirectUrl, 303);
  }
}

async function requestPatchProfile(cookie, body) {
  return fetch(`${import.meta.env.API_URL}/profile`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Cookie: cookie,
    },
    body,
  });
}
