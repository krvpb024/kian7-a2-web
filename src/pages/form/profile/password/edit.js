import * as R from "ramda";

export async function POST({ request, redirect, locals }) {
  const data = await request.formData();

  try {
    const accountPasswordEditResponse = await R.compose(
      R.curry(requestPatchAccountPassword)(locals.cookieHeader),
      JSON.stringify,
      Object.fromEntries,
      Array.from
    )(data.entries());

    const accountPasswordEditResponseJsonData =
      accountPasswordEditResponse.json();
    console.log(await accountPasswordEditResponseJsonData);

    switch (accountPasswordEditResponse.status) {
      case 200:
        return redirect("/profile", 303);
      case 400:
        switch ((await accountPasswordEditResponseJsonData).message) {
          case "password incorrect":
            locals.setGlobalMessage(
              [
                {
                  type: "error",
                  message: "原密碼錯誤。",
                },
              ],
              {
                path: "/profile/password/edit",
              }
            );
            return redirect("/profile/password/edit", 303);
          case "password confirm not pass":
            locals.setGlobalMessage(
              [
                {
                  type: "error",
                  message: "新密碼與再次輸入新密碼不一致。",
                },
              ],
              {
                path: "/profile/password/edit",
              }
            );
            return redirect("/profile/password/edit", 303);
          default:
            locals.setGlobalMessage(locals.err500Message);
            return redirect("/profile/password/edit", 303);
        }
      case 401:
        locals.setGlobalMessage(locals.err401Message);
        return redirect("/sign_in", 303);
      default:
        throw new Error(
          `account password patch request success, but unexpected status ${accountPasswordEditResponse.status}.`
        );
    }
  } catch (err) {
    const redirectUrl = `/profile/password/edit`;
    console.error(err);
    locals.setGlobalMessage(
      [
        {
          type: "error",
          message:
            "修改密碼時伺服器錯誤。您可以再試一次，或是連絡我們，等待我們將問題排除",
        },
      ],
      {
        path: redirectUrl,
      }
    );
    return redirect(redirectUrl, 303);
  }
}

async function requestPatchAccountPassword(cookie, body) {
  return fetch(`${import.meta.env.API_URL}/account/password`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      Cookie: cookie,
    },
    body,
  });
}
