export async function POST({ request, redirect, locals }) {
  const data = await request.formData();

  try {
    const healthRecordsImportResponse = await fetch(
      `${import.meta.env.API_URL}/health_records`,
      {
        method: "POST",
        headers: {
          Cookie: locals.cookieHeader,
        },
        body: data,
      }
    );

    switch (healthRecordsImportResponse.status) {
      case 201:
        return redirect("/health_records", 303);
      case 400:
        locals.setGlobalMessage(locals.err500Message);
        return redirect("/health_records", 303);
      case 401:
        locals.setGlobalMessage(locals.err401Message);
        return redirect("/sign_in", 303);
      default:
        throw new Error(
          `health_records post request success, but unexpected status ${healthRecordsImportResponse.status}.`
        );
    }
  } catch (err) {
    const healthRecordsImportUrl = `/health_records/import`;
    console.error(err);
    locals.setGlobalMessage(
      [
        {
          type: "error",
          message:
            "匯入多筆健康記錄時伺服器錯誤。您可以再試一次，或是連絡我們，等待我們將問題排除",
        },
      ],
      {
        path: healthRecordsImportUrl,
      }
    );
    return redirect(healthRecordsImportUrl, 303);
  }
}
