export async function POST({ request, redirect, locals }) {
  const data = await request.formData();

  const signUpResponse = await fetch(
    `${import.meta.env.API_URL}/auth/sign_up`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: data.get("name"),
        password: data.get("password"),
        passwordConfirm: data.get("passwordConfirm"),
      }),
    }
  );

  switch (signUpResponse.status) {
    case 201:
      return redirect("/sign_in", 303);
    case 400: {
      const signUpResponseJsonData = await signUpResponse.json();
      if (signUpResponseJsonData.message === "account has been taken") {
        locals.setGlobalMessage(
          [{ type: "error", message: "帳號已被使用。" }],
          {
            path: "/sign_up",
          }
        );
        return redirect("/sign_up", 303);
      }
      break;
    }
    default:
      locals.setGlobalMessage(locals.err500Message, {
        path: "/sign_up",
      });
      return redirect("/sign_up", 303);
  }
}
