export async function POST({ redirect, locals }) {

  const signOutResponse = await fetch(
    `${import.meta.env.API_URL}/auth/sign_out`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Cookie: locals.cookieHeader,
      }
    }
  );
  locals.forwardSetCookieHeader(signOutResponse);

  switch (signOutResponse.status) {
    case 200:
      return redirect("/select_data_mode", 303);
    default:
      locals.setGlobalMessage(locals.err500Message, {
        path: "/sign_in",
      });

      return redirect("/sign_in", 303);
  }
}
