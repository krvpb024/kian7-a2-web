import * as R from "ramda";

export async function POST({ request, redirect, locals }) {
  try {
    const data = await request.formData();

    const healthRecordCreateResponse = await R.compose(
      R.curry(requestGetHealthRecords)(locals.cookieHeader),
      JSON.stringify,
      Object.fromEntries,
      R.map(
        R.compose(
          R.when(
            R.allPass([
              R.complement(R.propEq("date", 0)),
              R.propSatisfies(R.isNotNil, 1),
            ]),
            R.over(R.lensIndex(1), Number)
          ),
          R.when(R.propSatisfies(R.isEmpty, 1), R.set(R.lensIndex(1), null))
        )
      ),
      Array.from
    )(data.entries());

    switch (healthRecordCreateResponse.status) {
      case 201:
        return redirect("/health_records", 303);
      case 400:
        locals.setGlobalMessage(locals.err500Message);
        return redirect("/health_records", 303);
      case 401:
        locals.setGlobalMessage(locals.err401Message);
        return redirect("/sign_in", 303);
      default:
        throw new Error(
          `health_record post request success, but unexpected status ${healthRecordCreateResponse.status}.`
        );
    }
  } catch (err) {
    const healthRecordCreateUrl = "/health_record/create";
    console.error(err);
    locals.setGlobalMessage(
      [
        {
          type: "error",
          message:
            "建立健康記錄時伺服器錯誤。您可以再試一次，或是連絡我們，等待我們將問題排除",
        },
      ],
      {
        path: healthRecordCreateUrl,
      }
    );
    return redirect(healthRecordCreateUrl, 303);
  }
}

async function requestGetHealthRecords(cookie, body) {
  return fetch(`${import.meta.env.API_URL}/health_record`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Cookie: cookie,
    },
    body,
  });
}
