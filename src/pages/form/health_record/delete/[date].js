export async function POST({ params, redirect, locals }) {
  try {
    const healthRecordDeleteResponse = await fetch(
      `${import.meta.env.API_URL}/health_record/${params.date}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Cookie: locals.cookieHeader,
        },
      }
    );

    switch (healthRecordDeleteResponse.status) {
      case 200:
        return redirect("/health_records", 303);
      case 400:
        locals.setGlobalMessage(locals.err500Message);
        return redirect("/health_records", 303);
      case 401:
        locals.setGlobalMessage(locals.err401Message);
        return redirect("/sign_in", 303);
      default:
        throw new Error(
          `health_record delete request success, but unexpected status ${healthRecordDeleteResponse.status}.`
        );
    }
  } catch (err) {
    const healthRecordEditUrl = `/health_record/edit/${params.date}`;
    console.error(err);
    locals.setGlobalMessage(
      [
        {
          type: "error",
          message:
            "刪除健康記錄時伺服器錯誤。您可以再試一次，或是連絡我們，等待我們將問題排除",
        },
      ],
      {
        path: healthRecordEditUrl,
      }
    );
    return redirect(healthRecordEditUrl, 303);
  }
}
