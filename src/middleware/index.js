import { sequence } from "astro/middleware";
import { parse as setCookieParse } from "set-cookie-parser";
import { CLOUD_MODE } from "../utils/dataMode.js";
import { serialize as cookieSerialize } from "cookie";

export const onRequest = sequence(
  setGlobalMessageMiddleware,
  extractCookieHeaderFromRequestMiddleware,
  forwardSetCookieHeaderFromResponseMiddleware,
  setDataModeMiddleware,
  setProfileMiddleWare,
  setDateLocaleFormatMiddleware
);

function setGlobalMessageMiddleware({ locals, cookies }, next) {
  locals.setGlobalMessage = function setGlobalMessage(
    payload = [],
    cookieOptions = {
      maxAge: 1,
    }
  ) {
    cookies.set("globalMessages", JSON.stringify(payload), {
      maxAge: 1,
      path: "/",
      ...cookieOptions,
    });
  };

  locals.err500Message = [
    {
      type: "error",
      message: "伺服器錯誤，您可以再嘗試執行一次或等待我們將問題修正。",
    },
  ];

  locals.err401Message = [
    {
      type: "error",
      message: "必須先登入才能完成此操作。",
    },
  ];
  return next();
}

async function extractCookieHeaderFromRequestMiddleware(
  { request, locals },
  next
) {
  const requestCookie = request.headers.get("cookie");
  locals.cookieHeader = requestCookie;
  const response = await next();
  return response;
}

async function forwardSetCookieHeaderFromResponseMiddleware(
  { locals, cookies },
  next
) {
  locals.forwardSetCookieHeader = function forwardSetCookieHeader(
    fetchResponse
  ) {
    const responseSetCookie = setCookieParse(fetchResponse, {
      decodeValues: true,
    });

    if (responseSetCookie.length > 0) {
      responseSetCookie.forEach(
        function forwardSetCookieFromFetchResponseHeader({
          name,
          value,
          path,
          domain,
          maxAge,
          httpOnly,
          secure,
          sameSite,
        }) {
          cookies.set(name, value, {
            ...(path ? { path } : {}),
            ...(domain ? { domain } : {}),
            ...(maxAge ? { maxAge } : {}),
            ...(httpOnly ? { httpOnly } : {}),
            ...(secure ? { secure } : {}),
            ...(sameSite ? { sameSite } : {}),
          });
        }
      );
    }
  };
  const response = await next();
  return response;
}

async function setDataModeMiddleware({ locals, url }, next) {
  try {
    const currentUserResponse = await fetch(
      `${import.meta.env.API_URL}/account`,
      {
        method: "GET",
        headers: new Headers({
          Cookie: locals.cookieHeader,
        }),
      }
    );
    locals.forwardSetCookieHeader(currentUserResponse);

    const currentUserJsonData = await currentUserResponse.json();

    switch (currentUserResponse.status) {
      case 200:
        locals.user = currentUserJsonData.payload;
        locals.dataMode = CLOUD_MODE;
        break;
      case 401:
        switch (currentUserJsonData.message) {
          case "session key corrupted": {
            const headers = new Headers(currentUserResponse.headers);
            headers.append(
              "Set-Cookie",
              cookieSerialize(
                "globalMessages",
                JSON.stringify([
                  {
                    type: "error",
                    message: "無法確認您的帳號資訊，請重新登入。",
                  },
                ]),
                {
                  httpOnly: !import.meta.env.DEV,
                  secure: !import.meta.env.DEV,
                  maxAge: 1,
                  path: "/sign_in",
                }
              )
            );
            headers.append("Location", "/sign_in");

            return new Response(null, {
              status: 303,
              headers,
            });
          }
          default:
            break;
        }
        locals.user = null;
        break;
      default:
        console.error("currentUser request fail", currentUserResponse);
        locals.user = null;
    }
    return await next();
  } catch (err) {
    console.error("api request fail", err);
    locals.user = null;
    locals.setGlobalMessage(
      [
        {
          type: "error",
          message:
            "確認是否登入時伺服器錯誤，若您使用雲端模式，您可以連絡我們或等待我們將問題排除。若是使用檔案模式，這個錯誤不會影響使用。",
        },
      ],
      {
        path: url,
      }
    );
    return await next();
  }
}

async function setProfileMiddleWare({ locals, url }, next) {
  try {
    if (locals.dataMode !== CLOUD_MODE) return await next();

    const profileGetResponse = await fetch(
      `${import.meta.env.API_URL}/profile`,
      {
        method: "GET",
        headers: new Headers({
          Cookie: locals.cookieHeader,
        }),
      }
    );
    locals.forwardSetCookieHeader(profileGetResponse);

    const profileGetResponseJsonData = await profileGetResponse.json();
    locals.profile = profileGetResponseJsonData.payload;

    return await next();
  } catch (err) {
    console.error("profile get request fail", err);
    locals.user = null;
    locals.setGlobalMessage(locals.err500Message, {
      path: url,
    });
    return await next();
  }
}

function setDateLocaleFormatMiddleware({ locals }, next) {
  locals.getLocaleDate = function getLocaleDate(date) {
    return new Intl.DateTimeFormat("cmn-hant-tw", {
      dateStyle: "long",
      timeStyle: "short",
      timeZone: "Asia/Taipei",
    })
      .format(new Date(date))
      .split(" ")[0];
  };
  return next();
}
