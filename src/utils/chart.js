import * as R from "ramda";
import compareDesc from "date-fns/fp/compareDesc/index.js";
import addDays from "date-fns/fp/addDays/index.js";
import differenceInDays from "date-fns/fp/differenceInDays/index.js";
import formatISO from "date-fns/fp/formatISO/index.js";

export function genChart(
  dataDistanceWidth,
  height,
  maybeFilterFrom,
  maybeFilterTo,
  indicatorType,
  maybeHealthIndicatorRange,
  healthRecords,
) {
  const svgWidth = getSvgWidth(dataDistanceWidth, healthRecords);
  const svgHeight = height;
  const svgViewBox = getSvgViewBox(svgWidth, height, 20, 50, 50, 50);
  const xAxesCoords = null;
  const xAxisLineCoords = null;
  const yAxisLineCoords = null;
  const yAxisLineMinMaxValue = null;
  const dropLineCoords = null;
  const healthRecordValuesCoords = null;
  const healthIndicatorName = null;
  const healthIndicatorRange = null;

  return {
    svgWidth,
    svgHeight,
    svgViewBox,
    xAxesCoords,
    xAxisLineCoords,
    yAxisLineCoords,
    yAxisLineMinMaxValue,
    dropLineCoords,
    healthRecordValuesCoords,
    healthIndicatorName,
    healthIndicatorRange,
  };
}
export function getSvgWidth(dataDistanceWidth, healthRecords) {
  return dataDistanceWidth * healthRecords.size;
}

export function getSvgViewBox(
  width,
  height,
  topPadding,
  rightPadding,
  bottomPadding,
  leftPadding,
) {
  return [
    [
      R.unless(R.equals(0), R.negate)(leftPadding),
      R.unless(R.equals(0), R.negate)(topPadding),
    ],
    [width + rightPadding + leftPadding, height + bottomPadding + topPadding],
  ];
}

export function getXBaseCoords(width, height) {
  return [0, height];
}

export function getXMaxCoords(width, height) {
  return [width, height];
}

export function getYBaseCoords(width, height) {
  return getXBaseCoords(width, height);
}

export function getYMaxCoords(width, height) {
  return [0, 0];
}

export function getXAxesCoords(width, height, days) {
  const [xBaseCoordsX, xBaseCoordsY] = getXBaseCoords(width, height);
  return R.addIndex(R.map)(getCoordsByDay, days);

  function getCoordsByDay(day, i) {
    return [
      day,
      [xBaseCoordsX + (i / (days.length - 1)) * width, xBaseCoordsY],
    ];
  }
}

export function ceilingMinFloorMaxHealthRecordsValue([lower, upper]) {
  return [
    R.compose(
      R.multiply(10),
      Math.floor,
      R.divide(R.__, 10),
      R.max(0),
      R.subtract(R.__, 10),
    )(lower),
    R.compose(R.multiply(10), Math.ceil, R.divide(R.__, 10), R.add(10))(upper),
  ];
}

export function getMinMaxHealthRecordsValue(
  maybeRange,
  mapDayAndMaybeHealthRecordTableValue,
) {
  if (R.isEmpty(Array.from(mapDayAndMaybeHealthRecordTableValue))) return null;
  const arrayDayValue = R.filter(R.isNotNil, [
    ...mapDayAndMaybeHealthRecordTableValue.values(),
  ]);

  const minValue = R.reduce(R.min, arrayDayValue[0], arrayDayValue);
  const maxValue = R.reduce(R.max, arrayDayValue[0], arrayDayValue);
  const [rangeLowerBound, rangeUpperBound] = maybeRange ?? [minValue, maxValue];
  const chartMinValue = R.isNil(rangeLowerBound)
    ? minValue
    : R.min(rangeLowerBound, minValue);

  const chartMaxValue = R.isNil(rangeUpperBound)
    ? maxValue
    : R.max(rangeUpperBound, maxValue);
  return ceilingMinFloorMaxHealthRecordsValue([chartMinValue, chartMaxValue]);
}

export function getPaddedHealthRecordTables(
  maybeFilterFrom,
  maybeFilterTo,
  healthRecords,
) {
  if (R.isEmpty(healthRecords)) return new Map();
  const allDate = R.compose(
    R.map((date) => new Date(date)),
    R.when(R.always(R.isNotNil(maybeFilterTo)), R.append(maybeFilterTo)),
    R.when(R.always(R.isNotNil(maybeFilterFrom)), R.append(maybeFilterFrom)),
    R.map(R.prop("date")),
    R.filter(R.isNotNil),
  )(healthRecords);

  const allDateMap = R.compose(
    R.reduce((acc, v) => acc.set(v, null), new Map()),
    R.curry(getPadDate)(7),
  )(allDate);

  const healthRecordsMap = R.reduce(
    (map, healthRecord) => {
      const { date } = healthRecord;
      return map.set(formatISO(date), healthRecord);
    },
    new Map(),
    healthRecords,
  );

  const allDateHealthRecordsMap = new Map(
    R.sortBy(R.nth(0))([
      ...allDateMap.entries(),
      ...healthRecordsMap.entries(),
    ]),
  );

  return allDateHealthRecordsMap;

  function getPadDate(minLength, dates) {
    if (R.isEmpty(dates)) return [];
    const firstDate = R.reduce(R.min, R.nth(0, dates), dates);
    const lastDate = R.reduce(R.max, R.nth(0, dates), dates);
    const firstLastDiff = differenceInDays(firstDate, lastDate);
    const fromFirstToLastDates = R.times(
      (n) => addDays(n)(firstDate),
      firstLastDiff + 1,
    );

    const distanceToMaxLength = minLength - R.length(fromFirstToLastDates);
    if (R.lt(distanceToMaxLength, 0))
      return R.map(formatISO, fromFirstToLastDates);

    const padToDate = addDays(distanceToMaxLength, lastDate);
    const firstPadDiff = differenceInDays(firstDate, padToDate);

    const fromFirstToPadDates = R.times(
      (n) => addDays(n)(firstDate),
      firstPadDiff + 1,
    );

    const result = R.compose(
      R.map(formatISO),
      R.sort(compareDesc),
    )(fromFirstToPadDates);
    return result;
  }
}

export function getAllValueByIndicator(
  healthRecordIndicator,
  mapDayMaybeHealthRecord,
) {
  if (
    R.isNil(mapDayMaybeHealthRecord) ||
    R.isNil(healthRecordIndicator) ||
    R.isEmpty([...mapDayMaybeHealthRecord.entries()])
  )
    return new Map();

  if (R.equals("bmi", healthRecordIndicator))
    return R.reduce(
      (map, [date, healthRecord]) => {
        if (R.isNil(healthRecord)) {
          map.set(date, null);
        } else {
          const { heightCm, weightKg } = healthRecord;
          map.set(date, weightKg / Math.pow(heightCm / 100, 2));
        }
        return map;
      },
      new Map(),
      mapDayMaybeHealthRecord.entries(),
    );

  return R.reduce(
    (map, [date, healthRecord]) => {
      map.set(date, healthRecord?.[healthRecordIndicator] ?? null);
      return map;
    },
    new Map(),
    mapDayMaybeHealthRecord.entries(),
  );
}

export function getYAxesCoordByValue(minValue, maxValue, height, value) {
  const percentage = (value - minValue) / (maxValue - minValue);
  return height - height * percentage;
}

export function getRangeCoords(
  xBaseCoordX,
  xMaxCoordX,
  maybeMinMaxValue,
  height,
  maybeRange,
) {
  if (R.isNil(maybeMinMaxValue)) return null;
  if (R.isNil(maybeRange)) return null;
  const [minValue, maxValue] = maybeMinMaxValue;
  const [lowerBound, upperBound] = maybeRange;
  const curryGetYAxesCoordByValue = R.curry(getYAxesCoordByValue);
  const getYAxesCoordsByMinMax = curryGetYAxesCoordByValue(
    minValue,
    maxValue,
    height,
  );
  const getCoords = R.cond([
    [
      R.all(R.isNotNil),
      R.always([
        [
          lowerBound,
          [
            [xBaseCoordX, getYAxesCoordsByMinMax(lowerBound)],
            [xMaxCoordX, getYAxesCoordsByMinMax(lowerBound)],
          ],
          true,
        ],
        [
          upperBound,
          [
            [xBaseCoordX, getYAxesCoordsByMinMax(upperBound)],
            [xMaxCoordX, getYAxesCoordsByMinMax(upperBound)],
          ],
          true,
        ],
      ]),
    ],
    [
      R.where({ 0: R.isNil, 1: R.isNotNil }),
      R.always([
        [
          minValue,
          [
            [xBaseCoordX, getYAxesCoordsByMinMax(minValue)],
            [xMaxCoordX, getYAxesCoordsByMinMax(minValue)],
          ],
          false,
        ],
        [
          upperBound,
          [
            [xBaseCoordX, getYAxesCoordsByMinMax(upperBound)],
            [xMaxCoordX, getYAxesCoordsByMinMax(upperBound)],
          ],
          true,
        ],
      ]),
    ],
    [
      R.where({ 0: R.isNotNil, 1: R.isNil }),
      R.always([
        [
          lowerBound,
          [
            [xBaseCoordX, getYAxesCoordsByMinMax(lowerBound)],
            [xMaxCoordX, getYAxesCoordsByMinMax(lowerBound)],
          ],
          true,
        ],
        [
          maxValue,
          [
            [xBaseCoordX, getYAxesCoordsByMinMax(maxValue)],
            [xMaxCoordX, getYAxesCoordsByMinMax(maxValue)],
          ],
          false,
        ],
      ]),
    ],
    [R.T, R.always(null)],
  ]);
  return getCoords(maybeRange);
}

export function getYAxesCoords(
  height,
  maybeMinMaxValue,
  xAxesCoordsWithDay,
  mapHealthRecords,
) {
  if (R.isEmpty(xAxesCoordsWithDay)) return [];
  if (R.isEmpty([...mapHealthRecords.entries()])) return [];
  if (R.isNil(maybeMinMaxValue)) return [];

  const [minValue, maxValue] = maybeMinMaxValue;

  const maybeCoordsMap = R.map(([day, value]) => {
    return R.ifElse(
      R.isNil,
      R.always([day, null]),
      R.always([
        day,
        [
          value,
          [
            R.nth(0)(
              getXAxesCoordByDay(xAxesCoordsWithDay, mapHealthRecords, day),
            ),
            getYAxesCoordByValue(minValue, maxValue, height, value),
          ],
        ],
      ]),
    )(value);
  })([...mapHealthRecords.entries()]);
  return maybeCoordsMap;

  function getXAxesCoordByDay(dayCoords, dayHealthRecordMap, day) {
    const dayIndex = [...dayHealthRecordMap.keys()].indexOf(day);
    const resultDayCoords = R.o(R.nth(1), R.nth(dayIndex))(dayCoords);
    return resultDayCoords;
  }
}

export function getYAxesCoordsWithInfo(
  yAxesMaybeCoords,
  maybeHealthIndicatorRange,
) {
  if (R.isEmpty(yAxesMaybeCoords)) return [];
  const [result] = R.reduce(reduceFn, [[], null], yAxesMaybeCoords);
  return result;

  function reduceFn(
    [chartValues, maybeLastValueDateAndLastValue],
    [currentDate, maybeCurrentValueAndCurrentCoords],
  ) {
    if (
      R.all(R.isNil, [
        maybeLastValueDateAndLastValue,
        maybeCurrentValueAndCurrentCoords,
      ])
    ) {
      return [
        R.append(
          {
            date: currentDate,
            valueData: null,
          },
          chartValues,
        ),
        null,
      ];
    } else if (
      R.and(
        R.isNil(maybeLastValueDateAndLastValue),
        R.isNotNil(maybeCurrentValueAndCurrentCoords),
      )
    ) {
      const [currentValue, currentCoords] = maybeCurrentValueAndCurrentCoords;
      return [
        R.append(
          {
            date: currentDate,
            valueData: {
              value: currentValue,
              coords: currentCoords,
              rangeComparison: R.isNotNil(maybeHealthIndicatorRange)
                ? rangeCompare(currentValue, maybeHealthIndicatorRange)
                : null,
              info: null,
            },
          },
          chartValues,
        ),
        [currentDate, currentValue],
      ];
    } else if (
      R.all(R.isNotNil, [
        maybeLastValueDateAndLastValue,
        maybeCurrentValueAndCurrentCoords,
      ])
    ) {
      const [currentValue, currentCoords] = maybeCurrentValueAndCurrentCoords;
      const [lastValueDate, lastValue] = maybeLastValueDateAndLastValue;

      return [
        R.append(
          {
            date: currentDate,
            valueData: {
              value: currentValue,
              coords: currentCoords,
              rangeComparison: R.isNotNil(maybeHealthIndicatorRange)
                ? rangeCompare(currentValue, maybeHealthIndicatorRange)
                : null,
              info: {
                diffDay: differenceInDays(lastValueDate)(currentDate),
                value: currentValue - lastValue,
              },
            },
          },
          chartValues,
        ),
        [currentDate, currentValue],
      ];
    } else if (
      R.and(
        R.isNotNil(maybeLastValueDateAndLastValue),
        R.isNil(maybeCurrentValueAndCurrentCoords),
      )
    ) {
      const [lastValueDate, lastValue] = maybeLastValueDateAndLastValue;
      return [
        R.append(
          {
            date: currentDate,
            valueData: null,
          },
          chartValues,
        ),
        [lastValueDate, lastValue],
      ];
    } else {
      return [
        [chartValues, maybeLastValueDateAndLastValue],
        [currentDate, maybeCurrentValueAndCurrentCoords],
      ];
    }
  }

  function rangeCompare(value, range) {
    const result = R.cond([
      [R.curry(isBelowRange), R.always([true, false, false])],
      [R.curry(isInRange), R.always([false, true, false])],
      [R.curry(isAboveRange), R.always([false, false, true])],
    ])(range, value);

    return result;

    function isInRange([lowerBound, upperBound], value) {
      if (R.all(R.isNotNil, [lowerBound, upperBound])) {
        return lowerBound <= value && value < upperBound;
      } else if (R.and(R.isNil(lowerBound), R.isNotNil(upperBound))) {
        return value < upperBound;
      } else if (R.and(R.isNotNil(lowerBound), R.isNil(upperBound))) {
        return value >= lowerBound;
      } else {
        return false;
      }
    }
    function isBelowRange([lowerBound, upperBound], value) {
      if (R.all(R.isNotNil, [lowerBound, upperBound])) {
        return value < lowerBound;
      } else if (R.and(R.isNotNil(lowerBound), R.isNil(upperBound))) {
        return value < lowerBound;
      } else {
        return false;
      }
    }
    function isAboveRange([lowerBound, upperBound], value) {
      if (R.all(R.isNotNil, [lowerBound, upperBound])) {
        return value >= upperBound;
      } else if (R.and(R.isNil(lowerBound), R.isNotNil(upperBound))) {
        return value >= upperBound;
      } else {
        return false;
      }
    }
  }
}
