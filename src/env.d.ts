/// <reference types="astro/client" />
declare namespace App {
  interface Locals {
    user: {
      name: string;
      id: number;
    };
    profile: {
      gender: string;
      birthDate: date;
    };
    dataMode: string;
    cookieHeader: AstroCookie;
    err500Message: [
      {
        type: string;
        message: string;
      }
    ];
    err401Message: [
      {
        type: string;
        message: string;
      }
    ];
    forwardSetCookieHeader: () => void;
    setGlobalMessage: (err500Message) => void;
    getLocaleDate: (string) => string;
  }
}
