import { describe } from "riteway/esm/riteway.js";
import * as R from "ramda";
import formatISOWithOptions from "date-fns/fp/formatISOWithOptions/index.js";
import formatISO from "date-fns/fp/formatISO/index.js";
import {
  getSvgWidth,
  getSvgViewBox,
  getXBaseCoords,
  getXMaxCoords,
  getYBaseCoords,
  getYMaxCoords,
  getXAxesCoords,
  ceilingMinFloorMaxHealthRecordsValue,
  getMinMaxHealthRecordsValue,
  getPaddedHealthRecordTables,
  getAllValueByIndicator,
  getYAxesCoordByValue,
  getRangeCoords,
  getYAxesCoords,
  getYAxesCoordsWithInfo,
} from "../src/utils/chart.js";

describe("getSvgWidth", async (assert) => {
  const given = (distance, size) =>
    `distance width ${distance} and size ${size} health records`;
  const should = "calculate whole svg width";

  {
    const testVar = [30, new Map([[new Date("2023-05-01"), null]])];
    assert({
      given: given(testVar[0], testVar[1].size),
      should,
      actual: getSvgWidth(...testVar),
      expected: 30,
    });
  }

  {
    const testVar = [
      30,
      new Map([
        [
          new Date("2023-05-01"),
          {
            date: "2023-05-01",
            heightCm: 184,
            weightKg: 76,
            bodyFatPercentage: 30,
            waistlineCm: 20,
          },
        ],
        [new Date("2023-05-02"), null],
        [
          new Date("2023-05-03"),
          {
            date: "2023-05-03",
            heightCm: 184,
            weightKg: 76,
            bodyFatPercentage: 30,
            waistlineCm: 20,
          },
        ],
        [new Date("2023-05-04"), null],
      ]),
    ];

    assert({
      given: given(testVar[0], testVar[1].size),
      should,
      actual: getSvgWidth(...testVar),
      expected: 120,
    });
  }
  {
    const testVar = [
      50,
      new Map([
        [
          new Date("2023-05-01"),
          {
            date: "2023-05-01",
            heightCm: 184,
            weightKg: 76,
            bodyFatPercentage: 30,
            waistlineCm: 20,
          },
        ],
        [new Date("2023-05-02"), null],
        [
          new Date("2023-05-03"),
          {
            date: "2023-05-03",
            heightCm: 184,
            weightKg: 76,
            bodyFatPercentage: 30,
            waistlineCm: 20,
          },
        ],
        [new Date("2023-05-04"), null],
      ]),
    ];
    assert({
      given: given(testVar[0], testVar[1].size),
      should,
      actual: getSvgWidth(...testVar),
      expected: 200,
    });
  }

  {
    const testVar = [
      100,
      new Map([
        [new Date("2023-05-01"), null],
        [new Date("2023-05-02"), null],
        [new Date("2023-05-03"), null],
        [new Date("2023-05-04"), null],
        [new Date("2023-05-05"), null],
      ]),
    ];
    assert({
      given: given(testVar[0], testVar[1].size),
      should,
      actual: getSvgWidth(...testVar),
      expected: 500,
    });
  }
});

describe("getSvgViewBox", async (assert) => {
  const given = (w, h, pt, pr, pb, pl) =>
    `svg width ${w}, height ${h}, padding top ${pt}, right ${pr}, bottom ${pb}, left ${pl}`;
  const should = "calculate whole svg width";

  {
    const testVar = [1600, 900, 20, 30, 20, 30];
    assert({
      given: given(...testVar),
      should,
      actual: getSvgViewBox(...testVar),
      expected: [
        [-30, -20],
        [1660, 940],
      ],
    });
  }

  {
    const testVar = [1400, 300, 0, 15, 4, 50];
    assert({
      given: given(...testVar),
      should,
      actual: getSvgViewBox(...testVar),
      expected: [
        [-50, 0],
        [1465, 304],
      ],
    });
  }

  {
    const testVar = [1234, 345, 24, 86, 19, 92];
    assert({
      given: given(...testVar),
      should,
      actual: getSvgViewBox(...testVar),
      expected: [
        [-92, -24],
        [1412, 388],
      ],
    });
  }

  {
    const testVar = [430, 1000, 0, 1, 2, 3];
    assert({
      given: given(...testVar),
      should,
      actual: getSvgViewBox(...testVar),
      expected: [
        [-3, 0],
        [434, 1002],
      ],
    });
  }

  {
    const testVar = [2169, 1280, 4, 77, 85, 111];
    assert({
      given: given(...testVar),
      should,
      actual: getSvgViewBox(...testVar),
      expected: [
        [-111, -4],
        [2357, 1369],
      ],
    });
  }
});

describe("getXBaseCoords", async (assert) => {
  const given = (w, h) => `width ${w}, height ${h}`;
  const should = "return base coords of x axis";

  {
    const testVar = [1600, 900];
    assert({
      given: given(...testVar),
      should,
      actual: getXBaseCoords(...testVar),
      expected: [0, 900],
    });
  }

  {
    const testVar = [1400, 300];
    assert({
      given: given(...testVar),
      should,
      actual: getXBaseCoords(...testVar),
      expected: [0, 300],
    });
  }

  {
    const testVar = [1234, 345];
    assert({
      given: given(...testVar),
      should,
      actual: getXBaseCoords(...testVar),
      expected: [0, 345],
    });
  }

  {
    const testVar = [430, 1000];
    assert({
      given: given(...testVar),
      should,
      actual: getXBaseCoords(...testVar),
      expected: [0, 1000],
    });
  }

  {
    const testVar = [2169, 1280];
    assert({
      given: given(...testVar),
      should,
      actual: getXBaseCoords(...testVar),
      expected: [0, 1280],
    });
  }
});

describe("getXMaxCoords", async (assert) => {
  const given = (w, h) => `width ${w}, height ${h}`;
  const should = "return max coords of x axis";

  {
    const testVar = [1600, 900];
    assert({
      given: given(...testVar),
      should,
      actual: getXMaxCoords(...testVar),
      expected: [1600, 900],
    });
  }

  {
    const testVar = [1400, 300];
    assert({
      given: given(...testVar),
      should,
      actual: getXMaxCoords(...testVar),
      expected: [1400, 300],
    });
  }

  {
    const testVar = [1234, 345];
    assert({
      given: given(...testVar),
      should,
      actual: getXMaxCoords(...testVar),
      expected: [1234, 345],
    });
  }

  {
    const testVar = [430, 1000];
    assert({
      given: given(...testVar),
      should,
      actual: getXMaxCoords(...testVar),
      expected: [430, 1000],
    });
  }

  {
    const testVar = [2169, 1280];
    assert({
      given: given(...testVar),
      should,
      actual: getXMaxCoords(...testVar),
      expected: [2169, 1280],
    });
  }
});

describe("getYBaseCoords", async (assert) => {
  const given = (w, h) => `width ${w}, height ${h}`;
  const should = "return base coords of y axis";

  {
    const testVar = [1600, 900];
    assert({
      given: given(...testVar),
      should,
      actual: getYBaseCoords(...testVar),
      expected: [0, 900],
    });
  }

  {
    const testVar = [1400, 300];
    assert({
      given: given(...testVar),
      should,
      actual: getYBaseCoords(...testVar),
      expected: [0, 300],
    });
  }

  {
    const testVar = [1234, 345];
    assert({
      given: given(...testVar),
      should,
      actual: getYBaseCoords(...testVar),
      expected: [0, 345],
    });
  }

  {
    const testVar = [430, 1000];
    assert({
      given: given(...testVar),
      should,
      actual: getYBaseCoords(...testVar),
      expected: [0, 1000],
    });
  }

  {
    const testVar = [2169, 1280];
    assert({
      given: given(...testVar),
      should,
      actual: getYBaseCoords(...testVar),
      expected: [0, 1280],
    });
  }
});

describe("getYMaxCoords", async (assert) => {
  const given = (w, h) => `width ${w}, height ${h}`;
  const should = "return max coords of y axis";

  {
    const testVar = [1600, 900];
    assert({
      given: given(...testVar),
      should,
      actual: getYMaxCoords(...testVar),
      expected: [0, 0],
    });
  }

  {
    const testVar = [1400, 300];
    assert({
      given: given(...testVar),
      should,
      actual: getYMaxCoords(...testVar),
      expected: [0, 0],
    });
  }

  {
    const testVar = [1234, 345];
    assert({
      given: given(...testVar),
      should,
      actual: getYMaxCoords(...testVar),
      expected: [0, 0],
    });
  }

  {
    const testVar = [430, 1000];
    assert({
      given: given(...testVar),
      should,
      actual: getYMaxCoords(...testVar),
      expected: [0, 0],
    });
  }

  {
    const testVar = [2169, 1280];
    assert({
      given: given(...testVar),
      should,
      actual: getYMaxCoords(...testVar),
      expected: [0, 0],
    });
  }
});

describe("getXAxesCoords", async (assert) => {
  const given = (w, h, ds) =>
    `width ${w}, height ${h}, length ${ds.length} days`;
  const should = "return array of day, coords tuple";

  {
    const testVar = [2169, 1280, []];
    assert({
      given: given(...testVar),
      should,
      actual: getXAxesCoords(...testVar),
      expected: [],
    });
  }

  {
    const testVar = [
      1600,
      900,
      [
        new Date("2023-05-01"),
        new Date("2023-05-02"),
        new Date("2023-05-03"),
        new Date("2023-05-04"),
      ],
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getXAxesCoords(...testVar),
      expected: [
        [new Date("2023-05-01"), [(0 / 3) * 1600, 900]],
        [new Date("2023-05-02"), [(1 / 3) * 1600, 900]],
        [new Date("2023-05-03"), [(2 / 3) * 1600, 900]],
        [new Date("2023-05-04"), [(3 / 3) * 1600, 900]],
      ],
    });
  }

  {
    const testVar = [
      1400,
      300,
      [
        new Date("2023-05-01"),
        new Date("2023-06-02"),
        new Date("2023-07-03"),
        new Date("2023-08-04"),
      ],
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getXAxesCoords(...testVar),
      expected: [
        [new Date("2023-05-01"), [(0 / 3) * 1400, 300]],
        [new Date("2023-06-02"), [(1 / 3) * 1400, 300]],
        [new Date("2023-07-03"), [(2 / 3) * 1400, 300]],
        [new Date("2023-08-04"), [(3 / 3) * 1400, 300]],
      ],
    });
  }

  {
    const testVar = [
      1234,
      345,
      [
        new Date("2023-05-04"),
        new Date("2023-06-03"),
        new Date("2023-07-02"),
        new Date("2023-08-01"),
      ],
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getXAxesCoords(...testVar),
      expected: [
        [new Date("2023-05-04"), [(0 / 3) * 1234, 345]],
        [new Date("2023-06-03"), [(1 / 3) * 1234, 345]],
        [new Date("2023-07-02"), [(2 / 3) * 1234, 345]],
        [new Date("2023-08-01"), [(3 / 3) * 1234, 345]],
      ],
    });
  }

  {
    const testVar = [
      430,
      1000,
      [new Date("2022-05-04"), new Date("2023-08-01")],
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getXAxesCoords(...testVar),
      expected: [
        [new Date("2022-05-04"), [(0 / 1) * 430, 1000]],
        [new Date("2023-08-01"), [(1 / 1) * 430, 1000]],
      ],
    });
  }
});

describe("ceilingMinFloorMaxHealthRecordsValue", async (assert) => {
  const given = ([lower, upper]) =>
    `tuple of lower ${lower} and upper ${upper} bound`;
  const should = "return padded lower and upper bound";

  {
    const testVar = [22, 33];
    assert({
      given: given(testVar),
      should,
      actual: ceilingMinFloorMaxHealthRecordsValue(testVar),
      expected: [10, 50],
    });
  }

  {
    const testVar = [0, 40];
    assert({
      given: given(testVar),
      should,
      actual: ceilingMinFloorMaxHealthRecordsValue(testVar),
      expected: [0, 50],
    });
  }

  {
    const testVar = [-20, 123];
    assert({
      given: given(testVar),
      should,
      actual: ceilingMinFloorMaxHealthRecordsValue(testVar),
      expected: [0, 140],
    });
  }

  {
    const testVar = [9, 24];
    assert({
      given: given(testVar),
      should,
      actual: ceilingMinFloorMaxHealthRecordsValue(testVar),
      expected: [0, 40],
    });
  }

  {
    const testVar = [40, 50];
    assert({
      given: given(testVar),
      should,
      actual: ceilingMinFloorMaxHealthRecordsValue(testVar),
      expected: [30, 60],
    });
  }
});

describe("getMinMaxHealthRecordsValue", async (assert) => {
  const given = (range, dayValueMap) => {
    const [lower, upper] = range ?? [];
    return `range lower bound ${lower}, upper bound ${upper} and day value map`;
  };
  const should = "return min max health record value";

  {
    const testVar = [[9, 30], new Map()];
    assert({
      given: given(...testVar),
      should,
      actual: getMinMaxHealthRecordsValue(...testVar),
      expected: null,
    });
  }

  {
    const testVar = [
      [9, 30],
      new Map([
        [new Date("2023-05-01"), 30],
        [new Date("2023-05-02"), 25],
        [new Date("2023-05-03"), null],
        [new Date("2023-05-04"), 83],
      ]),
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getMinMaxHealthRecordsValue(...testVar),
      expected: [0, 100],
    });
  }
  {
    const testVar = [
      [40, 100],
      new Map([
        [new Date("2023-05-01"), 30],
        [new Date("2023-05-02"), 25],
        [new Date("2023-05-03"), null],
        [new Date("2023-05-04"), 83],
      ]),
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getMinMaxHealthRecordsValue(...testVar),
      expected: [10, 110],
    });
  }
  {
    const testVar = [
      null,
      new Map([
        [new Date("2023-05-01"), 30],
        [new Date("2023-05-02"), 25],
        [new Date("2023-05-03"), null],
        [new Date("2023-05-04"), 83],
      ]),
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getMinMaxHealthRecordsValue(...testVar),
      expected: [10, 100],
    });
  }
  {
    const testVar = [
      [9, null],
      new Map([
        [new Date("2023-05-01"), 30],
        [new Date("2023-05-02"), 25],
        [new Date("2023-05-03"), null],
        [new Date("2023-05-04"), 83],
      ]),
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getMinMaxHealthRecordsValue(...testVar),
      expected: [0, 100],
    });
  }

  {
    const testVar = [
      [40, null],
      new Map([
        [new Date("2023-05-01"), 30],
        [new Date("2023-05-02"), 25],
        [new Date("2023-05-03"), null],
        [new Date("2023-05-04"), 83],
      ]),
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getMinMaxHealthRecordsValue(...testVar),
      expected: [10, 100],
    });
  }
  {
    const testVar = [
      [null, 193],
      new Map([
        [new Date("2023-05-01"), 30],
        [new Date("2023-05-02"), 25],
        [new Date("2023-05-03"), null],
        [new Date("2023-05-04"), 83],
      ]),
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getMinMaxHealthRecordsValue(...testVar),
      expected: [10, 210],
    });
  }
  {
    const testVar = [
      [null, 65],
      new Map([
        [new Date("2023-05-01"), 30],
        [new Date("2023-05-02"), 25],
        [new Date("2023-05-03"), null],
        [new Date("2023-05-04"), 83],
      ]),
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getMinMaxHealthRecordsValue(...testVar),
      expected: [10, 100],
    });
  }
});

describe("getPaddedHealthRecordTables", async (assert) => {
  const given = (from, to, healthRecords) => {
    const formatDate = R.ifElse(
      R.isNotNil,
      R.compose(
        formatISOWithOptions({ representation: "date" }),
        (d) => new Date(d),
      ),
      R.identity,
    );
    return `from ${formatDate(from)}, to ${formatDate(to)}, health records`;
  };
  const should = "return map day and health records in filter range";
  const testHealthRecords = [
    {
      date: new Date("2023-05-01"),
      heightCm: 184,
      weightKg: 76,
      bodyFatPercentage: 30,
      waistlineCm: 20,
    },
    {
      date: new Date("2023-05-03"),
      heightCm: 184,
      weightKg: 74,
      bodyFatPercentage: 21,
      waistlineCm: null,
    },
    {
      date: new Date("2023-05-04"),
      heightCm: 184,
      weightKg: 72,
      bodyFatPercentage: 25,
      waistlineCm: 26,
    },
    {
      date: new Date("2023-05-07"),
      heightCm: 184,
      weightKg: 70,
      bodyFatPercentage: null,
      waistlineCm: 23,
    },
  ];

  {
    const testVar = [null, null, []];
    assert({
      given: given(...testVar),
      should,
      actual: getPaddedHealthRecordTables(...testVar),
      expected: new Map(),
    });
  }

  {
    const testVar = [new Date("2023-04-29"), new Date("2023-05-10"), []];
    assert({
      given: given(...testVar),
      should,
      actual: getPaddedHealthRecordTables(...testVar),
      expected: new Map(),
    });
  }

  {
    const testVar = [null, null, testHealthRecords];
    assert({
      given: given(...testVar),
      should,
      actual: getPaddedHealthRecordTables(...testVar),
      expected: new Map([
        [
          formatISO(new Date("2023-05-01")),
          {
            date: new Date("2023-05-01"),
            heightCm: 184,
            weightKg: 76,
            bodyFatPercentage: 30,
            waistlineCm: 20,
          },
        ],
        [formatISO(new Date("2023-05-02")), null],
        [
          formatISO(new Date("2023-05-03")),
          {
            date: new Date("2023-05-03"),
            heightCm: 184,
            weightKg: 74,
            bodyFatPercentage: 21,
            waistlineCm: null,
          },
        ],
        [
          formatISO(new Date("2023-05-04")),
          {
            date: new Date("2023-05-04"),
            heightCm: 184,
            weightKg: 72,
            bodyFatPercentage: 25,
            waistlineCm: 26,
          },
        ],
        [formatISO(new Date("2023-05-05")), null],
        [formatISO(new Date("2023-05-06")), null],
        [
          formatISO(new Date("2023-05-07")),
          {
            date: new Date("2023-05-07"),
            heightCm: 184,
            weightKg: 70,
            bodyFatPercentage: null,
            waistlineCm: 23,
          },
        ],
      ]),
    });
  }

  {
    const testVar = [new Date("2023-04-29"), null, testHealthRecords];
    assert({
      given: given(...testVar),
      should,
      actual: getPaddedHealthRecordTables(...testVar),
      expected: new Map([
        [formatISO(new Date("2023-04-29")), null],
        [formatISO(new Date("2023-04-30")), null],
        [
          formatISO(new Date("2023-05-01")),
          {
            date: new Date("2023-05-01"),
            heightCm: 184,
            weightKg: 76,
            bodyFatPercentage: 30,
            waistlineCm: 20,
          },
        ],
        [formatISO(new Date("2023-05-02")), null],
        [
          formatISO(new Date("2023-05-03")),
          {
            date: new Date("2023-05-03"),
            heightCm: 184,
            weightKg: 74,
            bodyFatPercentage: 21,
            waistlineCm: null,
          },
        ],
        [
          formatISO(new Date("2023-05-04")),
          {
            date: new Date("2023-05-04"),
            heightCm: 184,
            weightKg: 72,
            bodyFatPercentage: 25,
            waistlineCm: 26,
          },
        ],
        [formatISO(new Date("2023-05-05")), null],
        [formatISO(new Date("2023-05-06")), null],
        [
          formatISO(new Date("2023-05-07")),
          {
            date: new Date("2023-05-07"),
            heightCm: 184,
            weightKg: 70,
            bodyFatPercentage: null,
            waistlineCm: 23,
          },
        ],
      ]),
    });
  }

  {
    const testVar = [null, new Date("2023-05-10"), testHealthRecords];
    assert({
      given: given(...testVar),
      should,
      actual: getPaddedHealthRecordTables(...testVar),
      expected: new Map([
        [
          formatISO(new Date("2023-05-01")),
          {
            date: new Date("2023-05-01"),
            heightCm: 184,
            weightKg: 76,
            bodyFatPercentage: 30,
            waistlineCm: 20,
          },
        ],
        [formatISO(new Date("2023-05-02")), null],
        [
          formatISO(new Date("2023-05-03")),
          {
            date: new Date("2023-05-03"),
            heightCm: 184,
            weightKg: 74,
            bodyFatPercentage: 21,
            waistlineCm: null,
          },
        ],
        [
          formatISO(new Date("2023-05-04")),
          {
            date: new Date("2023-05-04"),
            heightCm: 184,
            weightKg: 72,
            bodyFatPercentage: 25,
            waistlineCm: 26,
          },
        ],
        [formatISO(new Date("2023-05-05")), null],
        [formatISO(new Date("2023-05-06")), null],
        [
          formatISO(new Date("2023-05-07")),
          {
            date: new Date("2023-05-07"),
            heightCm: 184,
            weightKg: 70,
            bodyFatPercentage: null,
            waistlineCm: 23,
          },
        ],
        [formatISO(new Date("2023-05-08")), null],
        [formatISO(new Date("2023-05-09")), null],
        [formatISO(new Date("2023-05-10")), null],
      ]),
    });
  }

  {
    const testVar = [
      new Date("2023-04-29"),
      new Date("2023-05-10"),
      testHealthRecords,
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getPaddedHealthRecordTables(...testVar),
      expected: new Map([
        [formatISO(new Date("2023-04-29")), null],
        [formatISO(new Date("2023-04-30")), null],
        [
          formatISO(new Date("2023-05-01")),
          {
            date: new Date("2023-05-01"),
            heightCm: 184,
            weightKg: 76,
            bodyFatPercentage: 30,
            waistlineCm: 20,
          },
        ],
        [formatISO(new Date("2023-05-02")), null],
        [
          formatISO(new Date("2023-05-03")),
          {
            date: new Date("2023-05-03"),
            heightCm: 184,
            weightKg: 74,
            bodyFatPercentage: 21,
            waistlineCm: null,
          },
        ],
        [
          formatISO(new Date("2023-05-04")),
          {
            date: new Date("2023-05-04"),
            heightCm: 184,
            weightKg: 72,
            bodyFatPercentage: 25,
            waistlineCm: 26,
          },
        ],
        [formatISO(new Date("2023-05-05")), null],
        [formatISO(new Date("2023-05-06")), null],
        [
          formatISO(new Date("2023-05-07")),
          {
            date: new Date("2023-05-07"),
            heightCm: 184,
            weightKg: 70,
            bodyFatPercentage: null,
            waistlineCm: 23,
          },
        ],
        [formatISO(new Date("2023-05-08")), null],
        [formatISO(new Date("2023-05-09")), null],
        [formatISO(new Date("2023-05-10")), null],
      ]),
    });
  }
});

describe("getAllValueByIndicator", async (assert) => {
  const given = (indicatorName) =>
    `indicator ${indicatorName} and map day health record`;
  const should = "return day of indicator value map";

  const testHealthRecords = new Map([
    [
      formatISO(new Date("2023-05-01")),
      {
        date: new Date("2023-05-01"),
        heightCm: 184,
        weightKg: 76,
        bodyFatPercentage: 30,
        waistlineCm: 20,
      },
    ],
    [formatISO(new Date("2023-05-02")), null],
    [
      formatISO(new Date("2023-05-03")),
      {
        date: new Date("2023-05-03"),
        heightCm: 184,
        weightKg: 74,
        bodyFatPercentage: 21,
        waistlineCm: null,
      },
    ],
    [
      formatISO(new Date("2023-05-04")),
      {
        date: new Date("2023-05-04"),
        heightCm: 184,
        weightKg: 72,
        bodyFatPercentage: 25,
        waistlineCm: 26,
      },
    ],
    [formatISO(new Date("2023-05-05")), null],
    [formatISO(new Date("2023-05-06")), null],
    [
      formatISO(new Date("2023-05-07")),
      {
        date: new Date("2023-05-07"),
        heightCm: 184,
        weightKg: 70,
        bodyFatPercentage: null,
        waistlineCm: 23,
      },
    ],
  ]);

  {
    const testVar = ["bmi", testHealthRecords];
    assert({
      given: given(...testVar),
      should,
      actual: getAllValueByIndicator(...testVar),
      expected: new Map([
        [formatISO(new Date("2023-05-01")), 76 / Math.pow(184 / 100, 2)],
        [formatISO(new Date("2023-05-02")), null],
        [formatISO(new Date("2023-05-03")), 74 / Math.pow(184 / 100, 2)],
        [formatISO(new Date("2023-05-04")), 72 / Math.pow(184 / 100, 2)],
        [formatISO(new Date("2023-05-05")), null],
        [formatISO(new Date("2023-05-06")), null],
        [formatISO(new Date("2023-05-07")), 70 / Math.pow(184 / 100, 2)],
      ]),
    });
  }

  {
    const testVar = ["bodyFatPercentage", testHealthRecords];
    assert({
      given: given(...testVar),
      should,
      actual: getAllValueByIndicator(...testVar),
      expected: new Map([
        [formatISO(new Date("2023-05-01")), 30],
        [formatISO(new Date("2023-05-02")), null],
        [formatISO(new Date("2023-05-03")), 21],
        [formatISO(new Date("2023-05-04")), 25],
        [formatISO(new Date("2023-05-05")), null],
        [formatISO(new Date("2023-05-06")), null],
        [formatISO(new Date("2023-05-07")), null],
      ]),
    });
  }

  {
    const testVar = ["waistlineCm", testHealthRecords];
    assert({
      given: given(...testVar),
      should,
      actual: getAllValueByIndicator(...testVar),
      expected: new Map([
        [formatISO(new Date("2023-05-01")), 20],
        [formatISO(new Date("2023-05-02")), null],
        [formatISO(new Date("2023-05-03")), null],
        [formatISO(new Date("2023-05-04")), 26],
        [formatISO(new Date("2023-05-05")), null],
        [formatISO(new Date("2023-05-06")), null],
        [formatISO(new Date("2023-05-07")), 23],
      ]),
    });
  }

  {
    const testVar = ["weightKg", testHealthRecords];
    assert({
      given: given(...testVar),
      should,
      actual: getAllValueByIndicator(...testVar),
      expected: new Map([
        [formatISO(new Date("2023-05-01")), 76],
        [formatISO(new Date("2023-05-02")), null],
        [formatISO(new Date("2023-05-03")), 74],
        [formatISO(new Date("2023-05-04")), 72],
        [formatISO(new Date("2023-05-05")), null],
        [formatISO(new Date("2023-05-06")), null],
        [formatISO(new Date("2023-05-07")), 70],
      ]),
    });
  }
});

describe("getYAxesCoordByValue", async (assert) => {
  const given = (minV, maxV, h, v) =>
    `min value ${minV}, max value ${maxV}, height ${h} and value ${v}`;
  const should = "return Y axes coord";

  {
    const testVar = [50, 100, 600, 70];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoordByValue(...testVar),
      expected: 600 - 600 * ((70 - 50) / (100 - 50)),
    });
  }

  {
    const testVar = [23, 134, 400, 25];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoordByValue(...testVar),
      expected: 400 - 400 * ((25 - 23) / (134 - 23)),
    });
  }

  {
    const testVar = [93, 520, 646, 363];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoordByValue(...testVar),
      expected: 646 - 646 * ((363 - 93) / (520 - 93)),
    });
  }

  {
    const testVar = [124, 346, 942, 124];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoordByValue(...testVar),
      expected: 942 - 942 * ((124 - 124) / (346 - 124)),
    });
  }

  {
    const testVar = [1, 7, 600, 7];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoordByValue(...testVar),
      expected: 600 - 600 * ((7 - 1) / (7 - 1)),
    });
  }
});

describe("getRangeCoords", async (assert) => {
  const given = (
    xBaseCoordX,
    xMaxCoordX,
    maybeMinMaxValue,
    height,
    maybeRange,
  ) =>
    `x base coord x ${xBaseCoordX}, x max coord x ${xMaxCoordX}, min max value ${maybeMinMaxValue}, height ${height} and range ${maybeRange}`;
  const should = "return range coords";

  {
    const testVar = [0, 1000, null, 600, [9, 30]];
    assert({
      given: given(...testVar),
      should,
      actual: getRangeCoords(...testVar),
      expected: null,
    });
  }

  {
    const testVar = [0, 1000, [50, 100], 600, [9, 30]];
    assert({
      given: given(...testVar),
      should,
      actual: getRangeCoords(...testVar),
      expected: [
        [
          9,
          [
            [0, getYAxesCoordByValue(50, 100, 600, 9)],
            [1000, getYAxesCoordByValue(50, 100, 600, 9)],
          ],
          true,
        ],
        [
          30,
          [
            [0, getYAxesCoordByValue(50, 100, 600, 30)],
            [1000, getYAxesCoordByValue(50, 100, 600, 30)],
          ],
          true,
        ],
      ],
    });
  }

  {
    const testVar = [0, 1000, [50, 100], 600, [null, 12]];
    assert({
      given: given(...testVar),
      should,
      actual: getRangeCoords(...testVar),
      expected: [
        [
          50,
          [
            [0, getYAxesCoordByValue(50, 100, 600, 50)],
            [1000, getYAxesCoordByValue(50, 100, 600, 50)],
          ],
          false,
        ],
        [
          12,
          [
            [0, getYAxesCoordByValue(50, 100, 600, 12)],
            [1000, getYAxesCoordByValue(50, 100, 600, 12)],
          ],
          true,
        ],
      ],
    });
  }

  {
    const testVar = [0, 1000, [50, 100], 600, [2, null]];
    assert({
      given: given(...testVar),
      should,
      actual: getRangeCoords(...testVar),
      expected: [
        [
          2,
          [
            [0, getYAxesCoordByValue(50, 100, 600, 2)],
            [1000, getYAxesCoordByValue(50, 100, 600, 2)],
          ],
          true,
        ],
        [
          100,
          [
            [0, getYAxesCoordByValue(50, 100, 600, 100)],
            [1000, getYAxesCoordByValue(50, 100, 600, 100)],
          ],
          false,
        ],
      ],
    });
  }
});

describe("getYAxesCoords", async (assert) => {
  const given = (h, minMaxValue) =>
    `height ${h}, min ${minMaxValue?.[0]} max ${minMaxValue?.[1]} x axes coords with day and health records`;
  const should = "return array of day, indicator value and coords";

  {
    const testVar = [
      600,
      null,
      [[new Date("2023-05-01"), [0, 0]]],
      new Map([[new Date("2023-05-01"), 23]]),
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoords(...testVar),
      expected: [],
    });
  }

  {
    const testVar = [
      600,
      [20, 50],
      [],
      new Map([[new Date("2023-05-01"), 23]]),
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoords(...testVar),
      expected: [],
    });
  }

  {
    const testVar = [
      600,
      [20, 50],
      [[new Date("2023-05-01"), [0, 0]]],
      new Map(),
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoords(...testVar),
      expected: [],
    });
  }

  {
    const testVar = [
      600,
      [20, 50],
      [
        [new Date("2023-05-01"), [2, 3]],
        [new Date("2023-05-02"), [0, 0]],
        [new Date("2023-05-03"), [4, 5]],
      ],
      new Map([
        [new Date("2023-05-01"), 23],
        [new Date("2023-05-02"), null],
        [new Date("2023-05-03"), 49],
      ]),
    ];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoords(...testVar),
      expected: [
        [
          new Date("2023-05-01"),
          [23, [2, getYAxesCoordByValue(20, 50, 600, 23)]],
        ],
        [new Date("2023-05-02"), null],
        [
          new Date("2023-05-03"),
          [49, [4, getYAxesCoordByValue(20, 50, 600, 49)]],
        ],
      ],
    });
  }
});

describe("getYAxesCoordsWithInfo", async (assert) => {
  const given = (yAxesMaybeCoords, range) =>
    `day, maybe value, coords tuple and range lower bound ${range?.[0]} upper bound ${range?.[1]}`;
  const should = "return range status";

  const testInput = [
    [new Date("2023-05-01"), null],
    [new Date("2023-05-02"), [1, [3, 4]]],
    [new Date("2023-05-03"), null],
    [new Date("2023-05-04"), [5, [6, 7]]],
    [new Date("2023-05-05"), null],
    [new Date("2023-05-06"), null],
    [new Date("2023-05-07"), null],
    [new Date("2023-05-08"), [8, [9, 10]]],
    [new Date("2023-05-09"), [22, [12, 13]]],
    [new Date("2023-05-10"), null],
  ];

  {
    const testVar = [[], null];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoordsWithInfo(...testVar),
      expected: [],
    });
  }

  {
    const testVar = [testInput, null];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoordsWithInfo(...testVar),
      expected: [
        { date: new Date("2023-05-01"), valueData: null },
        {
          date: new Date("2023-05-02"),
          valueData: {
            value: 1,
            coords: [3, 4],
            rangeComparison: null,
            info: null,
          },
        },
        { date: new Date("2023-05-03"), valueData: null },
        {
          date: new Date("2023-05-04"),
          valueData: {
            value: 5,
            coords: [6, 7],
            rangeComparison: null,
            info: { diffDay: 2, value: 4 },
          },
        },
        { date: new Date("2023-05-05"), valueData: null },
        { date: new Date("2023-05-06"), valueData: null },
        { date: new Date("2023-05-07"), valueData: null },
        {
          date: new Date("2023-05-08"),
          valueData: {
            value: 8,
            coords: [9, 10],
            rangeComparison: null,
            info: { diffDay: 4, value: 3 },
          },
        },
        {
          date: new Date("2023-05-09"),
          valueData: {
            value: 22,
            coords: [12, 13],
            rangeComparison: null,
            info: { diffDay: 1, value: 14 },
          },
        },
        { date: new Date("2023-05-10"), valueData: null },
      ],
    });
  }

  {
    const testVar = [testInput, [3, 10]];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoordsWithInfo(...testVar),
      expected: [
        { date: new Date("2023-05-01"), valueData: null },
        {
          date: new Date("2023-05-02"),
          valueData: {
            value: 1,
            coords: [3, 4],
            rangeComparison: [true, false, false],
            info: null,
          },
        },
        { date: new Date("2023-05-03"), valueData: null },
        {
          date: new Date("2023-05-04"),
          valueData: {
            value: 5,
            coords: [6, 7],
            rangeComparison: [false, true, false],
            info: { diffDay: 2, value: 4 },
          },
        },
        { date: new Date("2023-05-05"), valueData: null },
        { date: new Date("2023-05-06"), valueData: null },
        { date: new Date("2023-05-07"), valueData: null },
        {
          date: new Date("2023-05-08"),
          valueData: {
            value: 8,
            coords: [9, 10],
            rangeComparison: [false, true, false],
            info: { diffDay: 4, value: 3 },
          },
        },
        {
          date: new Date("2023-05-09"),
          valueData: {
            value: 22,
            coords: [12, 13],
            rangeComparison: [false, false, true],
            info: { diffDay: 1, value: 14 },
          },
        },
        { date: new Date("2023-05-10"), valueData: null },
      ],
    });
  }

  {
    const testVar = [testInput, [null, 10]];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoordsWithInfo(...testVar),
      expected: [
        { date: new Date("2023-05-01"), valueData: null },
        {
          date: new Date("2023-05-02"),
          valueData: {
            value: 1,
            coords: [3, 4],
            rangeComparison: [false, true, false],
            info: null,
          },
        },
        { date: new Date("2023-05-03"), valueData: null },
        {
          date: new Date("2023-05-04"),
          valueData: {
            value: 5,
            coords: [6, 7],
            rangeComparison: [false, true, false],
            info: { diffDay: 2, value: 4 },
          },
        },
        { date: new Date("2023-05-05"), valueData: null },
        { date: new Date("2023-05-06"), valueData: null },
        { date: new Date("2023-05-07"), valueData: null },
        {
          date: new Date("2023-05-08"),
          valueData: {
            value: 8,
            coords: [9, 10],
            rangeComparison: [false, true, false],
            info: { diffDay: 4, value: 3 },
          },
        },
        {
          date: new Date("2023-05-09"),
          valueData: {
            value: 22,
            coords: [12, 13],
            rangeComparison: [false, false, true],
            info: { diffDay: 1, value: 14 },
          },
        },
        { date: new Date("2023-05-10"), valueData: null },
      ],
    });
  }

  {
    const testVar = [testInput, [3, null]];
    assert({
      given: given(...testVar),
      should,
      actual: getYAxesCoordsWithInfo(...testVar),
      expected: [
        { date: new Date("2023-05-01"), valueData: null },
        {
          date: new Date("2023-05-02"),
          valueData: {
            value: 1,
            coords: [3, 4],
            rangeComparison: [true, false, false],
            info: null,
          },
        },
        { date: new Date("2023-05-03"), valueData: null },
        {
          date: new Date("2023-05-04"),
          valueData: {
            value: 5,
            coords: [6, 7],
            rangeComparison: [false, true, false],
            info: { diffDay: 2, value: 4 },
          },
        },
        { date: new Date("2023-05-05"), valueData: null },
        { date: new Date("2023-05-06"), valueData: null },
        { date: new Date("2023-05-07"), valueData: null },
        {
          date: new Date("2023-05-08"),
          valueData: {
            value: 8,
            coords: [9, 10],
            rangeComparison: [false, true, false],
            info: { diffDay: 4, value: 3 },
          },
        },
        {
          date: new Date("2023-05-09"),
          valueData: {
            value: 22,
            coords: [12, 13],
            rangeComparison: [false, true, false],
            info: { diffDay: 1, value: 14 },
          },
        },
        { date: new Date("2023-05-10"), valueData: null },
      ],
    });
  }
});
