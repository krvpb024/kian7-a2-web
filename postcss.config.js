import multiValueDisplay from "postcss-multi-value-display";
import nested from "postcss-nested";

const config = {
  plugins: [multiValueDisplay, nested],
};

export default config;
